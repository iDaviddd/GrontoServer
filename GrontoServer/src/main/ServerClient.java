package main;

import java.net.InetAddress;

public class ServerClient { 
	
	private String restaurant;
	private InetAddress address;
	private int port;
	private final int ID;
	private int attempt = 0;
	
	public ServerClient(String restaurant, InetAddress address, int port, final int ID) {
		this.restaurant = restaurant;
		this.address = address;
		this.port = port;
		this.ID = ID;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getAttempt() {
		return attempt;
	}
	
	public String getRestaurant() {
		return restaurant;
	}
	
	public InetAddress getAddress() {
		return address;
	}
	
	public int getPort() {
		return port;
	}
	
	public void addAttempt() {
		attempt += 1;
	}
	
	public void clearAttempts() {
		attempt = 0;
	}
	
}
