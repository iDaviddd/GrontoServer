package main;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Server implements Runnable {

	private List<ServerClient> clients = new ArrayList<ServerClient>();
	private List<Integer> clientResponse = new ArrayList<Integer>();

	private int port;
	private DatagramSocket socket;
	private boolean running = false;
	private boolean debug;

	private final int MAX_ATTEMPTS = 5;

	private Thread run, manage, send, receive;

	public Server(int port) {
		this.port = port;
		try {
			socket = new DatagramSocket(port);
		} catch (SocketException e) {
			e.printStackTrace();
			return;
		}
		run = new Thread(this, "Server");
		run.start();
	}
	
	/**
	 * Runs when the server is started and listens for commands. 
	 */
	public void run() {
		running = true;
		System.out.println("Server has been started on the port " + port);
		manageClients();
		receive();
		Scanner scan = new Scanner(System.in);
		while (running) {
			String input = scan.nextLine();
			if(!input.startsWith("/")) {
				printHelp(true);
			}
			input = input.substring(1);
			if(input.startsWith("debug")) {//Debug command.
				if(debug) {
					System.out.println("Debug mode turned off.");
				} else {
					System.out.println("Debug mode turned on.");
				}
				debug = !debug;
			} else if(input.equalsIgnoreCase("help")) {//Help command.
				printHelp(false);
			} else if(input.equalsIgnoreCase("online")) {//Clients command.
				System.out.println("Restaurants online: " + clients.size());
				for(int i = 0; i < clients.size(); i++) {
					ServerClient c = clients.get(i);
					int i2;
					i2 = i + 1;
					System.out.println("[" + i2 + "] " + c.getRestaurant() + "(" + c.getID() + ") @" + c.getAddress().toString() + ":" + c.getPort()); 
				}
			} else {
				printHelp(true);
			}
		}
		scan.close();
	}
	
	/**
	 * Print the help message.
	 * @param unknowncommand gives different printout if unknown command was entered.
	 */
	
	public void printHelp(boolean unknowncommand) {
		System.out.println("=========[GrontoServer by Grabbarna]==========");
		System.out.println("/help - print this Message.");
		System.out.println("/debug - enables debug mode. Shows all packages received.");
		System.out.println("/online - lists all of the online restaurants");
		System.out.println("==============================================");
		if (unknowncommand) System.out.println("The command you provided is known.");
	}

	/**
	 * Sends a ping to clients.
	 */
	private void manageClients() {
		manage = new Thread("Manage") {
			public void run() {
				while (running) {
					sendPingToAll();
					try {
						Thread.sleep(2 * 1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					for (int i = 0; i < clients.size(); i++) {
						ServerClient c = clients.get(i);
						if (!clientResponse.contains(c.getID())) {
							if (c.getAttempt() >= MAX_ATTEMPTS) {
								disconnect(c.getID(), false);
							} else {
								c.addAttempt();
							}
						} else {
							clientResponse.remove(new Integer(c.getID()));
							c.clearAttempts();
						}
					}
				}
			}
		};
		manage.start();
	}

	/**
	 * Receives data from restaurants.
	 */
	private void receive() {
		receive = new Thread("Receive") {
			public void run() {
				while (running) {
					byte[] data = new byte[1024];
					DatagramPacket packet = new DatagramPacket(data, data.length);
					try {
						socket.receive(packet);
					} catch (IOException e) {
						e.printStackTrace();
					}
					process(packet);
				}
			}
		};
		receive.start();
	}

	/**
	 * Processes the packet which contain information. Can be either a message
	 * or information about a new connection.
	 */
	private void process(DatagramPacket packet) {
		String string = new String(packet.getData());
		if(debug) System.out.println(string);
		if (string.startsWith("/d/")) { // This is a new client.
			int id = UniqueID.getID();
			String restaurantName = string.split("/d/|/e/")[1];
			ServerClient sc = new ServerClient(restaurantName, packet.getAddress(), packet.getPort(), id);
			clients.add(sc);
			String dataID = "/d/" + id;
			addEndAndSend(dataID, packet.getAddress(), packet.getPort());
			System.out.println("Restaurant: " + restaurantName + " @" + sc.getAddress().toString() + ":" + sc.getPort() +" has connected.");
		} else if (string.startsWith("/c/")) {//Handle disconnection.
			String id = string.split("/c/|/e/")[1];
			disconnect(Integer.parseInt(id), true);
		} else if (string.startsWith("/p/")) {//Handle ping.
			clientResponse.add(Integer.parseInt(string.split("/p/|/e/")[1]));
		} else {

		}
	}

	/**
	 * Disconnects client.
	 * 
	 * @param ID = ID of client
	 * @param status true = disconnect consciously, false = unconsciously.
	 */
	private void disconnect(int ID, boolean status) {
		ServerClient c = null;
		boolean existed = false;
		for (int i = 0; i < clients.size(); i++) {
			if (clients.get(i).getID() == ID) {
				c = clients.get(i);
				clients.remove(i);
				existed = true;
				break;
			}
		}

		if (!existed)
			return;

		String discmsg;
		if (status) {
			discmsg = c.getRestaurant() + " @" + c.getAddress().toString() + ":" + c.getPort() + " has disconnected.";
		} else {
			discmsg = c.getRestaurant() + " @" + c.getAddress().toString() + ":" + c.getPort() + " timed out.";
		}
		System.out.println(discmsg);
	}
	
	/**
	 * Sends a ping package to all the connected clients.
	 */
	private void sendPingToAll() {
		for(int i = 0; i < clients.size(); i++) {
			ServerClient client = clients.get(i);
			addEndAndSend("/p/server", client.getAddress(), client.getPort());
		}
	}

	/**
	 * Sends data to the target. 
	 * @param data Array of bytes to be sent. 
	 * @param address Target address.
	 * @param port Target port.
	 */
	private void send(final byte[] data, final InetAddress address, final int port) {
		send = new Thread("Send") {
			public void run() {
				DatagramPacket packet = new DatagramPacket(data, data.length, address, port);
				try {
					socket.send(packet);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		send.start();
	}

	/**
	 * Takes a string which is about to be sent, modifies it with ending string.
	 * @param message String which is to be edited.
	 * @param address Target address.
	 * @param port Target port.
	 */
	private void addEndAndSend(String message, InetAddress address, int port) {
		message += "/e/";
		send(message.getBytes(), address, port);
	}

}
